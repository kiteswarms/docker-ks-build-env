# Use an official ubuntu runtime as parent
FROM $BASE_IMAGE
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \
    && apt-get -qq update \
    && apt-get install --no-install-recommends -y \
        # python3-dev will defualt to 3.6 on ubuntu18 and 3.8 on ubuntu20
        python3-dev \ 
        git make\ 
        cmake ccache \
        doxygen \
        clang-tidy-10 valgrind \
        gcovr lcov\
        gcc g++ clang-10 \
        build-essential \
        # curl required by debian_dep_installer
        curl \
        # To be able to install dependencies-1.0.0.deb (run and build dependencies)
        gdebi-core \
        # elfutils and libelf-dev are required by abi-dumper
        libelf-dev elfutils \
        # The following packages are required by ctag
        pkg-config autoconf automake python3-docutils \
        libseccomp-dev libjansson-dev libyaml-dev libxml2-dev \ 
        default-jre libzmq5 libgtk-3-0 \
        # File is required by cpack to be able to generate debian packages
        file \
        python3-pip \
        # Common and frequent Kiteswarms dependencies that laregly increase
        libboost-all-dev \

    && rm -rf /var/lib/apt/lists/*


ADD https://github.com/ZeroCM/zcm/releases/download/v1.0.3/zcm_1.0.3_amd64.deb zcm_1.0.3_amd64.deb
RUN dpkg -i zcm_1.0.3_amd64.deb  && rm -rf zcm_1.0.3_amd64.deb

# Install Cppcheck from git as debian does not have newest version
# Is required to get full cpp17 support.
RUN git clone --branch=2.1 --depth=1 https://github.com/danmar/cppcheck.git \
    && cd cppcheck && mkdir build && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Release .. \
    && cmake --build . \
    && make install \
    && cd ../.. && rm -rf cppcheck

# The following installs abi-compliance-checker and abi-dumper with their dependencies
    # 1. vtable-dumper
 RUN git clone --branch=1.2 --depth=1 https://github.com/lvc/vtable-dumper.git \
    && cd vtable-dumper && make install prefix=/usr \
    && cd .. && rm -rf vtable-dumper \
    # 2. ctags
    && git clone --depth=1 https://github.com/universal-ctags/ctags.git \
    && cd ctags \
    && ./autogen.sh && ./configure && make && make install && cd .. && rm -rf ctags \
    # 3. abi-dumper
    && git clone --branch=1.1 --depth=1 https://github.com/lvc/abi-dumper.git \
    && cd abi-dumper && make install prefix=/usr \
    && cd .. && rm -rf abi-dumper \ 
    # 4. abi-compliance-checker
    && git clone --branch=2.3 --depth=1 https://github.com/lvc/abi-compliance-checker.git \
    && cd abi-compliance-checker && make install prefix=/usr \
    && cd .. && rm -rf abi-compliance-checker
# --------------------------------------------------------------------------------------------------
# get scm for automatic version generation
RUN pip3 install -U pip setuptools_scm 
