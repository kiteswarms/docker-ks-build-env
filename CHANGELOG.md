### Changed
- [Minor] Clang-tidy and Clang are updated to v10 for ubuntu 18 and 20
- [Major] Removed build-jobs for ubuntu18 as of Tuesday 2021-01-12
